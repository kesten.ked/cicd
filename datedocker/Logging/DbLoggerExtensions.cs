﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging.Configuration;

namespace SqlEfApi.Logging
{
    public static class DbLoggerExtensions
    {
        public static ILoggingBuilder AddDbLogger(this ILoggingBuilder builder, Action<DbLoggerConfiguration> configure)
        {
            // подключает конфигурацию
            builder.AddConfiguration();

            // добавляется только один раз если даже по ошибке добавили в Programs несоклько раз Logger
            // создает дискриптор только если он существует
            builder.Services.TryAddEnumerable(
                ServiceDescriptor.Singleton<ILoggerProvider, DbLoggerProvider>());

            // зарегистрируем options
            // когда provider требует конфигурацию, используй эту, DbLoggerConfiguration
            LoggerProviderOptions.RegisterProviderOptions<DbLoggerConfiguration, DbLoggerProvider>(builder.Services);

            builder.Services.Configure(configure);

            return builder;
        }
    }
}
