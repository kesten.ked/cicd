﻿using System.Globalization;

namespace datedocker.Services
{
    public class CalendarService
    {
        public string GetDayOfWeek(string date)
        {
            if (!DateTime.TryParse(date, out var day))
            {
                return "Не удалось получить дату";
            }

            var culture = new CultureInfo("ru-RU");
            return culture.DateTimeFormat.GetDayName(day.DayOfWeek);
        }
    }
}
